﻿$code = @"
using System;
using System.Security.Cryptography;
using System.Text;
namespace Tester
{
    public class Program
    {

        public static void Main()
        {
            var result = GetKeyWithPrefix("VS_123.pdf");

            Console.WriteLine(result);
        }


        public static string GetKeyWithPrefix(string key)
        {
            var _hasher = MD5.Create();
            var _syncLock = new object();
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] hashBytes;

            lock (_syncLock)
            {
                hashBytes = _hasher.ComputeHash(keyBytes);
            }

            string hashPrefix =
                hashBytes[0].ToString("x2")
                + hashBytes[1].ToString("x2")
                + "-";

            return hashPrefix + key;
        }

    }
}
"@
 
Add-Type -TypeDefinition $code -Language CSharp	
iex "[Tester.Program]::Main()"