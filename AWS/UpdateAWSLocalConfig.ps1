﻿Add-Type -AssemblyName System.Windows.Forms
$keysTofind = 'aws_access_key_id=|aws_secret_access_key=|aws_session_token=';
$userName = (Get-WMIObject -ClassName Win32_ComputerSystem).Username;
$userName = Split-Path -Path $userName -Leaf;
$awsCredentialFilePath = "C:\Users\$userName\.aws\credentials";

$keys = $keysTofind.Split("|");
[System.Windows.Forms.MessageBox]::Show("Are the AWS Credentials in your machine clipboard? Continue Task?","Update AWS Configuration", "YesNo" , "Information" , "Button1");
$awsConfigurationFileContent = Get-Content $awsCredentialFilePath;
$splittedContent = $awsConfigurationFileContent -Split ([Environment]::NewLine);
Write-Host "This tool will update the AWS credentials with the ones that you copied from the AWS console from the console";
$dataInClipboard = Get-Clipboard;
Write-Host "Starting update";
# loop through the keys to process and replace
for ($keyCounter = 0; $keyCounter -lt $keys.Length; $keyCounter++) {
  $currentCredential = $keys[$keyCounter];
  Write-Host "Processing:" $currentCredential;
  # read through the clipboard and get aws credentials
  for ($lineCounter = 0; $lineCounter -lt $dataInClipboard.Length; $lineCounter++) {
    $credentialFromClipboard = $dataInClipboard[$lineCounter];
    try {
      if ($credentialFromClipboard.StartsWith($currentCredential)) {
        for ($contentCounter = 0; $contentCounter -lt $splittedContent.Length; $contentCounter++) {
          $credentialValueToBeReplaced = $splittedContent[$contentCounter];
        
          if ($credentialValueToBeReplaced.StartsWith($currentCredential)) {
             (Get-Content $awsCredentialFilePath).replace($credentialValueToBeReplaced, $credentialFromClipboard ) | Set-Content $awsCredentialFilePath
          }
        }
      }
    }
    catch {
      Write-Host $_
      Write-Host "No Aws Credentials found in Clipboard";
      exit;
    }
  }
}
Write-Host "Update completed!";
